# Скрипт для сбора информации о свитче Cisco
Данный скрипт подключается к свитчу с Cisco IOS по telnet
(по умолчанию считается, что свитч расположен по адресу `localhost:5000`)
и выполняет на нём следующие команды:
* `show version`
* `show config`
* `show running-config`
* `show access-lists afi-all`
* `show interfaces`

Результат вывода каждой команды печатается в консоль.

## Как запустить?
1. Склонируйте проект
```
git clone <url>
```
Для получения `<url>` нажмите кнопку `Clone`

2. Переместитесь в папку проекта
```commandline
cd udv-test
```

3. Можно указать свои параметры подключения к свитчу (IP и порт) в файле `.env`  
Пример файла `.env` можно посмотреть в `.env.example`  
```commandline
nano .env
```

### Для запуска с использованием `Docker`
1. Создайте образ
```commandline
docker build --tag udv-test .
```
Убедиться, что образ создался, можно выполнив команду
```commandline
docker images
```
2. Запустите контейнер
```commandline
docker run --network host udv-test
```
Опция `--network host` позволяет контейнеру использовать одну сеть с хостом.

Вы увидите в консоли результат работы программы.

### Для запуска БЕЗ использования `Docker`
1. Создайте виртуальное окружение
```commandline
virtualenv venv
```
2. Активируйте виртуальное окружение в текущем терминале
```commandline
source venv/bin/activate
```
3. Установите зависимости проекта
```commandline
pip install -r requirements.txt
```
4. Запустите скрипт
```commandline
python src/main.py
```
Вы увидите в консоли результат работы программы.

## Устранение неполадок
Если что-то не работает, напишите мне в [телеграм](https://t.me/m3f3w_1337).