from typing import List

from netmiko import ConnectHandler
import settings


def get_connection() -> ConnectHandler:
    connection_properties = {
        'device_type': 'cisco_ios_telnet',
        'host': settings.HOST,
        'port': settings.PORT
    }
    return ConnectHandler(**connection_properties)


def print_output(info: str, output: str):
    print('=' * len(info))
    print(info)
    print('=' * len(info))
    print(output)
    print()


def main():
    net_connect: ConnectHandler = get_connection()
    # to see full output of commands
    net_connect.send_command('terminal length 0')
    commands: List[str] = [
        'show version',
        'show config',
        'show running-config',
        'show access-lists afi-all',
        'show interfaces'
    ]
    for command in commands:
        output = net_connect.send_command(command)
        info = f'Output of command \"{command}\":'
        print_output(info, output)


if __name__ == '__main__':
    main()
