from dotenv import find_dotenv, load_dotenv
import os

load_dotenv(find_dotenv())

HOST = os.getenv('HOST', 'localhost')
PORT = os.getenv('PORT', 5000)
